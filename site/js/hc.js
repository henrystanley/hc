
//////////////////
///// Utils //////
//////////////////

// Random num between n and m
function randN(n, m) {
  return n + Math.floor(Math.random() * (m-n));
}

// Removes element from array
function removeElement(arr, elem) {
  return arr.filter((v) => v != elem);
}

function leftPad(n, m) {
  return ("0".repeat(m) + n).substr(-1 * m, m);
}



///////////////////////////////
///// Hypercollage Object /////
///////////////////////////////

function Hypercollage(params={}) {

  ////////////
  /// Init ///
  ////////////

  // Properties
  this.name = params.name || 'hc';
  this.parent = params.parent || 'body'
  this.path = params.path || '';
  this.fileType = params.fileType || '.jpg';
  this.size = params.size || 0;
  this.min = params.min || 0;
  this.max = params.max || this.size;
  this.startSize = params.startSize || 1;
  this.sizeProb = params.sizeProb || 0.5;
  this.nextProb = params.nextProb || 1.0;
  this.fadeSpeed = params.fadeSpeed || 2000;
  this.nextSpeed = params.nextSpeed || 5000;
  this.visible = [];



  ///////////////////////////////////////
  /// Combination Animation Functions ///
  ///////////////////////////////////////

  // Get random visible image num
  this.randVisible = () => { return this.visible[randN(0, this.visible.length)]; }

  // Get random hidden image num
  this.randHidden = () => {
    var n = randN(0, this.size);
    while (this.visible.includes(n)) n = randN(0, this.size);
    return n;
  }

  // Show part
  this.showPart = (n) => {
    this.parts[n].velocity("fadeIn", { duration: this.fadeSpeed });
    this.visible.push(n);
  }

  // Hide part
  this.hidePart = (n) => {
    this.parts[n].velocity("fadeOut", { duration: this.fadeSpeed });
    this.visible = removeElement(this.visible, n);
  }

  // Show a random part
  this.randShow = () => { this.showPart(this.randHidden()); }

  // Hide a random part
  this.randHide = () => { this.hidePart(this.randVisible()); }

  // Randomly swap the visibility of two parts
  this.randSwap = () => {
   var n = this.randVisible();
   var m = this.randHidden();
   this.hidePart(n);
   this.showPart(m);
  }

  // Move to a new random parts configuration
  this.randConfig = () => {
    if (this.visible.length <= this.min) {
      if (Math.random() < this.sizeProb && this.max > 1) { this.randShow(); }
      else { this.randSwap(); }
    }
    else if (this.visible.length < this.max) {
      var n = Math.random();
      if (n < (this.sizeProb * 0.5)) { this.randHide(); }
      else if (n < (1.0 - (this.sizeProb * 0.5))) { this.randSwap(); }
      else { this.randShow(); }
    }
    else {
      if (Math.random() < this.sizeProb) { this.randHide(); }
      else { this.randSwap(); }
    }
  }

  // Returns a string representing the ID of the current part configuration
  this.configId = () => {
    var id = '';
    for (var i = 0; i < this.size; i++) {
      var sign = this.visible.includes(i) ? '+' : '-';
      id = id + sign;
    }
    return id;
  }


  //////////////////////
  /// Main Functions ///
  //////////////////////

  // Init
  this.init = () => {
    // Init container
    $(this.parent).prepend(`<div id="${this.name}" class="hypercollage">...</div>`);
    this.container = $(`#${this.name}`);

    // Add part elements
    this.parts = [];
    for (var i = 0; i < this.size; i++) {
      var e = `<img src="${this.path}${leftPad(i, `${this.size}`.length)}${this.fileType}" id="${this.name}_part_${i}">`;
      this.container.append(e);
      this.parts[i] = $(`#${this.name}_part_${i}`);
      this.parts[i].velocity("fadeOut", { duration: 0 });
    }

    // Init starting parts configuration
    for (var i = 0; i < this.startSize; i++) this.randShow();
  }

  // Starts an infinite loop calling nextRandConfig every nextSpeed
  this.start = () => {
    if (Math.random() < this.nextProb) this.randConfig();
    setTimeout(this.start, this.nextSpeed);
  }

}
