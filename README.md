# Hypercollages

The Hypercollage is a novel combinatoric artistic medium of my own invention.
Each one is a web-based generative art piece which utilizes a set of illustrations
or photographs (or both) which are composited stochastically in real time by a
small Javascript program. Parameters to this program can be used to precisely
control qualities of the resulting Hypercollage, such as the number of concurrently
composited elements or the speed at which they flash by.

[Read more about them on my website](https://istigkeit.xyz/Art/Hypercollages/About.html)

[View them online](https://hc.istigkeit.xyz/)


## Running/Deploying

This repo contains two small scripts I use to develop and deploy the Hypercollages,
which are essentially just plain static websites.

To develop the site locally I use:

    $ ./watch-build.sh

which can be used to monitor the `site` directory (using the wonderful [watchexec](https://watchexec.github.io/))
and copy the contents to `/var/www/hc` when any changes are made.

I just have `http://hc` mapped to `127.0.0.1` in my `/etc/hosts` file, and use a
simple Nginx config to serve the site contents out of `/var/www/hc`.

For production, I use:

    $ ./build-prod.sh

on my server to copy the site directory to `/www/hc.istigkeit.xyz` and give it
the appropriate permissions.


## Using the Hypercollage Library

I've separated the main logic and styling for the Hypercollages themselves
into the `site/js/hc.js` and `site/css/hc.css` files, in the hopes that this will
make it easier for others to create their own Hypercollages.

The Hypercollage script also depends on [jQuery](https://jquery.com/) and the
fantastic [Velocity](http://velocityjs.org/) library. The versions I use are included
in `site/js`.

To make a Hypercollage you'll need a directory of images of the same size and file type.
The script indexes images from 0 and expects appropriate padding, so if you have 19 images
the first should be `00.jpg` and the last `18.jpg`.

You can initialize a Hypercollage on your page with:

```js
var hc = new Hypercollage({
  name: 'my-hypercollage', // The name of this Hypercollage (defaults to 'hc')
  parent: '#hc-container', // The element your Hypercollage will be inserted into
  path: '/imgs', // The path for the images making up your Hypercollage
  fileType: '.png', // The file extension of your images (defaults to '.jpg')
  size: 21, // The number of images in your image directory
  min: 3, // The minimum number of images at any one time (defaults to 0)
  max: 8, // The maximum number of images at any one time (defaults to `size`)
  startSize: 7, // The number of images in the starting configuration (defaults to 1)
  sizeProb: 0.8, // The probability of changing the configuration size per tick (defaults to 0.5)
  nextProb: 0.8, // The probability of swapping/adding/removing an image each tick (defaults to 1)
  nextSpeed: 1500, // The time between ticks in milliseconds (defaults to 5000)
  fadeSpeed: 1000 // The time in milliseconds a changing image fades for (defaults to 2000)
});
hc.init() // Initialize the Hypercollage
hc.start(); // Start the Hypercollage

```

The contents of `site` should provide a pretty simple example of how everything
should be set up for this to work.
